from pyams import OPENDATALOGGER

# instance need serial parameters and datalogger type: CR1000, CR3000 or DOTLOGGER
campbell_cr1000 = OPENDATALOGGER.SERIALDATALOGGER(port='/dev/ttyUSB0', baudrate=115200,type="CR1000")
# start serial connection
campbell_cr1000.start_connection()
# destination path to save data downloaded & removed from datalogger
campbell_cr1000.get_stored_data("/home/emiliano/data.txt")
# Getting real-time data
realtime_data = campbell_cr1000.get_realtime_data()
# close serial connection
campbell_cr1000.close_connection()
# print realtime data
print(realtime_data)