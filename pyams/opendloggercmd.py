# README
# ======
# Este documento esta pensado para colocar en el los diferentes dataloggers
# junto con los comandos que se utiliza para obtener los datos
# Para esto se creara una lista con los diferentes comandos empezando por:
# - comando para obtener datos en tiempo real
# - comando para obtener todos los datos almacenados (contenido del datalogger)
#
# Campbell Scientific Datalogger CR1000/CR3000
#---------------------------------------------
# - Comando '7' obtiene los datos en tiempo real
# - Comando '8' obtiene los datos almacenados FLUX

# Stevens DOTLGGER
#-----------------
# - Comando 'CD' obtiene los datos en tiempo real
# - Comando 'DB' obtiene los datos almacenados

cmd = {'CR1000'   : ["7","8"],
       'CR3000'   : ["7","8"], 
       'DOTLOGGER': ["CD", "DB"],
      }