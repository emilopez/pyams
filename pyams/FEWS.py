import tkinter as tk
import os
import calendar
import re
import sys
import subprocess 
import ctypes
import logging
import wmi
import configparser as conf
import urllib.request
import pyautogui

from datetime import datetime as dt, date as da, time, timedelta
from time import sleep
from urllib.request import urlopen


root = tk.Tk()
time_str = tk.StringVar()
class STATION():
    
    global root
    global time_str
    
    def __init__(self, site, config_file='config.cfg'):
       	#cmd1='rsync.exe -avrPO --stats 192.168.111.122::openwrt-sah1 /cygdrive/c/Proyecto_Final/config/'
        #[status,output]=subprocess.getstatusoutput(cmd1)
        #print(cmd1)
        #print(output)
        self.site = site
        self.config_file = config_file
        self.cfg_parser = conf.ConfigParser()
        self.cfg_parser.readfp(open(config_file))
        if self.opcion_config == 0:
            self.get_config_from_web()
        logging.basicConfig(filename='sah.log', level=logging.INFO, format='%(asctime)s  - %(levelname)s -  %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.info('ON - Inicio de programa')
	
    def update_config_file(self):
        '''funcion que actualiza archivo de configuracion'''
        with open(self.config_file, "wb") as cfg_file:
            self.cfg_parser.write(cfg_file)

    
    def get_usuario(self):
        '''funcion que obtener el nombre de usuario'''
        return self.cfg_parser.getint("Usuario", "us")

    def set_usuario(self, nuevo):
        '''funcion actualiza de usuario'''
        self.cfg_parser.set("Usuario", "us", nuevo)
        self.update_config_file()

    usuario = property(get_usuario, set_usuario)
   
    def get_opcion(self):
        '''Función para obtener la opcion a ejecutar'''
        return self.cfg_parser.getint("opcion", "inicio")

    def set_opcion(self, nuevo):
        '''Función para actualizar la opcion a ejecutar: opcion a ejecutar 1 datos intantaneos, 2 datos diarios, 0 los dos'''
        self.cfg_parser.set("opcion", "inicio", nuevo)
        self.update_config_file()

    opcion_inicio = property(get_opcion, set_opcion)


    def get_tiempo_shutdown(self):
        '''Funcion para obtener el tiempo de espera (para apagarse) luego de ejecucion fuera de hora '''
        return self.cfg_parser.getint("tiempo_apagado", "tapaga")

    def set_tiempo_shutdown(self, nuevo):
        '''Funcion para actualizar el tiempo de espera (para apagarse) luego de ejecucion fuera de hora'''
        self.cfg_parser.set("tiempo_apagado", "tapaga", nuevo)
        self.update_config_file()

    tiempo_apagado = property(get_tiempo_shutdown, set_tiempo_shutdown)

    def get_tiempo_apagado2(self):
        '''Funcion para obtener el tiempo de espera (para apagarse) luego de ejecucion de una o varias tareas '''	
        return self.cfg_parser.getint("tiempo_apagado", "tapaga2")

    def set_tiempo_apagado2(self, nuevo):
        '''Funcion para actualizar el tiempo de espera (para apagarse) luego de ejecucion fuera de hora '''
        self.cfg_parser.set("tiempo_apagado", "tapaga2", nuevo)
        self.update_config_file()

    tiempo_apagado2 = property(get_tiempo_apagado2, set_tiempo_apagado2)

    def get_dias(self):
        '''Funcion para obtener la cantidad de dias anteriores (datos diarios)  '''
        return self.cfg_parser.getint("dias_ant", "dias")

    def set_dias(self, nuevo):
        '''Funcion para actualizar la cantidad de dias anteriores (datos diarios)  '''
        self.cfg_parser.set("dias_ant", "dias", nuevo)
        self.update_config_file()

    dias_atras = property(get_dias, set_dias)
   
    def get_hora_inicio_defecto(self):
        '''Funcion para obtener la hora de inicio de la/s tareas'''
        return self.cfg_parser.getint("Hora_Inicio_Defecto", "hora")

    def set_hora_defecto(self, nueva_hora):
        '''Funcion para actualizar la hora de inicio de la/s tareas'''
        self.cfg_parser.set("Hora_Inicio_Defecto", "hora", nueva_hora)
        self.update_config_file()

    hora_inicio_defecto = property(get_hora_inicio_defecto, set_hora_defecto)
    
    def get_minutos_inicio_defecto(self):
        '''Funcion para obtener los minutos de inicio de la/s tareas'''
        return self.cfg_parser.getint("Hora_Inicio_Defecto", "minutos")

    def set_minutos_defecto(self, nuevo_minutos):
        '''Funcion para actualizar los minutos de inicio de la/s tareas'''
        self.cfg_parser.set("Hora_Inicio_defecto", "minutos", nuevo_minutos)
        self.update_config_file()

    minutos_inicio_defecto = property(get_minutos_inicio_defecto, set_minutos_defecto)

    def get_delta_inicio_defecto(self):
        '''Funcion para obtener el tamaño de la ventana para la ejecucion de la/s tareas'''
        return self.cfg_parser.getint("Hora_Inicio_Defecto", "delta")

    def set_delta_defecto(self, nuevo_delta):
        '''Funcion para actualizar el tamaño de la ventana para la ejecucion de la/s tareas'''
        self.cfg_parser.set("Hora_Inicio_Defecto", "delta", nuevo_delta)
        self.update_config_file()

    delta_inicio_defecto = property(get_delta_inicio_defecto, set_delta_defecto)
  
    def get_hora_inicio(self):
        return self.cfg_parser.getint("Hora_Inicio", "hora")
    def set_hora(self, nueva_hora):
        self.cfg_parser.set("Hora_Inicio", "hora", nueva_hora)
        self.update_config_file()
    hora_inicio = property(get_hora_inicio, set_hora)
    
    def get_minutos_inicio(self):
        return self.cfg_parser.getint("Hora_Inicio", "minutos")
    def set_minutos(self, nuevo_minutos):
        self.cfg_parser.set("Hora_Inicio", "minutos", nuevo_minutos)
        self.update_config_file()
    minutos_inicio = property(get_minutos_inicio, set_minutos)
    
    def get_delta_inicio(self):
        return self.cfg_parser.getint("Hora_Inicio", "delta")
    def set_delta(self, nuevo_delta):
        self.cfg_parser.set("Hora_Inicio", "delta", nuevo_delta)
        self.update_config_file()
    delta_inicio = property(get_delta_inicio, set_delta)

    def get_pass_tecmes(self):
        '''Funcion para obtener la password del software de Tecmes, Tecmes por defecto'''
        return self.cfg_parser.get("Password", "pass")

    def set_pass_tecmes(self, nueva_pass):
        '''Funcion para actualizar obtener la password del software de Tecmes, previamente debe ser modificada en el software'''
        self.cfg_parser.set("Password", "pass", nueva_pass)
        self.update_config_file()

    pass_tecmes = property(get_pass_tecmes, set_pass_tecmes)

    def get_remote_user(self):
        '''Funcion para obtener el usuario remoto para enviar los datos por rsync'''
        return self.cfg_parser.get("envio_rsync", "RemoteUser")

    def set_remote_user(self, nuevo):
        '''Funcion para actualizar el usuario remoto para enviar los datos por rsync'''
        self.cfg_parser.set("envio_rsync", "RemoteUser", nuevo)
        self.update_config_file()

    remote_user = property(get_remote_user, set_remote_user)

    def get_remote_host(self):
        '''Funcion para obtener el nombre host remoto para enviar los datos por rsync'''
        return self.cfg_parser.get("envio_rsync", "RemoteHost")

    def set_remote_host(self, nuevo):
        '''Funcion para actualizar el nombre host remoto para enviar los datos por rsync'''
        self.cfg_parser.set("envio_rsync", "RemoteHost", nuevo)
        self.update_config_file()

    remote_host = property(get_remote_host, set_remote_host)

    def get_remote_pass(self):
        '''Funcion para obtener la ruta del archivo con la password para enviar los datos por rsync'''
        return self.cfg_parser.get("envio_rsync", "Password")

    def set_remote_pass(self, nuevo):
        '''Funcion para actualizar la ruta del archivo con la password para enviar los datos por rsync'''
        self.cfg_parser.set("envio_rsync", "Password", nuevo)
        self.update_config_file()

    remote_pass = property(get_remote_pass, set_remote_pass)

    def get_remote_localdir(self):
        '''Funcion para obtener la ruta del directorio local, para enviar los datos por rsync'''
        return self.cfg_parser.get("envio_rsync", "LocalDir")

    def set_remote_localdir(self, nuevo):
        '''Funcion para actualizar la ruta del directorio local, para enviar los datos por rsync'''
        self.cfg_parser.set("envio_rsync", "LocalDir", nuevo)
        self.update_config_file()

    remote_localdir = property(get_remote_localdir, set_remote_localdir)

    def get_remote_dir(self):
        '''Funcion para obtener la ruta del directorio remoto, para enviar los datos por rsync'''
        return self.cfg_parser.get("envio_rsync", "RemoteDir")

    def set_remote_dir(self, nuevo):
        '''Funcion para actulizar la ruta del directorio remoto, para enviar los datos por rsync'''
        self.cfg_parser.set("envio_rsync", "RemoteDir", nuevo)
        self.update_config_file()

    remote_dir = property(get_remote_dir, set_remote_dir)

    def get_log_file(self):
        '''Funcion para obtener la ruta del archivo de log de rsync'''
        return self.cfg_parser.get("envio_rsync", "LogFile")

    def set_log_file(self, nuevo):
        '''Funcion para actualizar la ruta del archivo de log de rsync'''
        self.cfg_parser.set("envio_rsync", "LogFile", nuevo)
        self.update_config_file()

    rsync_log_file = property(get_log_file, set_log_file)

    def get_opcion_config(self):
        '''Funcion para obtener la opción sobre el archivo de configuracion (config.cfg)'''
        return self.cfg_parser.get("config", "opcion_config")

    def set_opcion_config(self, nuevo):
        '''Funcion para actualizar la opción sobre el archivo de configuracion (config.cfg), , 0 lo descarga, otro no'''
        self.cfg_parser.set("config", "opcion_config", nuevo)
        self.update_config_file()

    opcion_config = property(get_opcion_config, set_opcion_config)
    
    def get_dir_icono(self):
        '''Funcion para obtener la ruta de la imagen del icono del software de la UTR'''
        return self.cfg_parser.get("icono", "dir")

    def set_dir_icono(self, nuevo):
        '''Funcion para actualizar la ruta de la imagen del icono del software de la UTR'''
        self.cfg_parser.set("icono", "dir", nuevo)
        self.update_config_file()

    ruta_icono = property(get_dir_icono, set_dir_icono)
    
    def upload_data_to_web(self, log_file=None, password_file=None, user=None, server=None, remote_dir=None, protocol='rscync'):
        '''Funcion que envia los datos a la web a traves de rsync'''
        if protocol == 'rsync':
            if log_file is None:
                log_file = self.rsync_log_file
            if password_file is None:
                password_file = self.remote_pass
            if user is None:
                user = self.remote_user
            if server is None:
                server = self.remote_host
            if remote_dir is None:
                remote_dir = self.remote_dir


	    #datos para el envio remoto
        #RemoteUser = 'emi'
        #RemoteHost = '174.136.4.6'
        #Pass = '/cygdrive/c/secret'
        #LocalDir= '/cygdrive/c/sah/sfe/'
        #RemoteDir  = 'sah-sfe'

        cmd1 = f"rsync.exe -avrPO --progress --exclude=*.py --exclude=*.cfg --msgs2stderr --log-file={log_file} --password-file={password_file} {user}@{server}::{remote_dir}"

        [_, output] = subprocess.getstatusoutput(cmd1)
        print(cmd1)
        print(output)
        
    def get_config_from_web(self):
        '''Funcion que recibe el archivo de configuración (config.cfg) desde la web a traves de rsync'''
        # datos para el envio remoto
        #RemoteUser = 'emi'
        #RemoteHost = '174.136.4.6'
        #Pass = '/cygdrive/c/secret'
        #LocalDir= '/cygdrive/c/sah/sfe/'
        #RemoteDir  = 'sah-sfe'

        cmd1 ='rsync.exe -avrPO --progress --exclude=*.py --exclude=*.cfg --password-file='+self.remote_pass + ' ' + self.remote_user+'@'+self.remote_host+'::'+self.remote_dir+' '+ self.remote_localdir

        [status,output] = subprocess.getstatusoutput(cmd1)
        print(cmd1)
        print(output)

    def shutdown(self, log = None):
        '''Funcion que apaga la pc luego de 5 o 1 minuto/s y, tambien se ejecuta 
           si se pospone el apagado, ademas deshabilita la comunicación con la UTR
        
        
        	Parameters
        	----------
        	self : variables para acceder a los atributos y metodos de la clase
        	log  : variable que indica lo que debe registrar el archivo log,  por defecto es None
        	     log=None ---> archivo log:5MIN - Inicio secuencia de apagado
        	     log=5min ---> archivo log:POS5MIN - Secuencia de apagado pospuesta 5 minutos
                     log=1min ---> archivo log:POS1MIN - Secuencia de apagado pospuesta 1 minuto
        
        	Returns
        	-------
        '''

        if log == None:
            logging.info('5MIN - Inicio secuencia de apagado')
            t_apagado=self.tiempo_apagado
        elif log == '5min':
            logging.info('POS5MIN - Secuencia de apagado pospuesta 5 minutos')
            t_apagado=self.tiempo_apagado
        elif log == '1min':
            logging.info('POS1MIN - Secuencia de apagado pospuesta 1 minuto')
            t_apagado=self.tiempo_apagado2
            
        self.upload_data_to_web()
        self.disable_serial_port()
        
        for t in range(t_apagado, -1, -1):
            # format as 2 digit integers, fills with zero to the left
            # divmod() gives minutes, seconds
            sf = "{:02d}:{:02d}".format(*divmod(t, 60))
            #print(sf)  # test
            time_str.set(sf)
            root.update()
            # delay one second
            sleep(1)
            if t == 0:
                logging.info('OFF - Apagado')
                time_str.set('Apagado')
                root.destroy()
                apagar = "shutdown /s"
                os.system(apagar)
                
    def get_realtime_data(self):
        '''- Enable serial communication
           - Get and save real-time data   
           - Disable serial communication
        '''

        #habilitar puerto serie
        self.enable_serial_port()

        pyautogui.FAILSAFE = False
        hoy = dt.now().strftime("%Y-%m-%d %H-%M-%S")
        hora_inicio= dt.now().strftime("%H-%M-%S")
        # abrir software utr mediante localizacion de icono en el escritorio
        soft = pyautogui.locateOnScreen(self.ruta_icono)
        softx, softy = pyautogui.center(soft)
        pyautogui.doubleClick(softx, softy)

        pyautogui.PAUSE = 5

        pyautogui.press("enter")
        pyautogui.typewrite(self.pass_tecmes)
        pyautogui.press("enter")

        #pyautogui.press("enter") #enter del error verificar si esta en la estacion de Santa fe
        pyautogui.press("enter") 
        sleep(10)
        # elegir opción datos
        pyautogui.press("down") 
        pyautogui.press("down") 
        pyautogui.press("enter")

        # tabs para llegar hasta el boton mostra datos 
        pyautogui.press("tab")
        pyautogui.press("tab")
        pyautogui.press("tab")
        pyautogui.press("tab")
        pyautogui.press("tab")
        pyautogui.press("tab")
        pyautogui.press("enter")
        sleep(10)
        pyautogui.press("enter")
        sleep(6)
        # guardar
        pyautogui.hotkey('ctrl', 'g')

        pyautogui.typewrite(hoy)
        pyautogui.press("enter")
        
        # salir del programa
        
        pyautogui.press("enter")
        pyautogui.hotkey('alt', 'f4')
        
        sleep(10)
        pyautogui.hotkey('alt', 'f4')
            
        hora_fin= dt.now().strftime("%H-%M-%S")
        hoy2=da.today()
        
        hoy2 = hoy2.strftime("%Y-%m-%d")
        
        if os.path.exists(hoy+'.txt'):
            logging.info('INST - Datos instantaneos')
        else:
            logging.critical('ERROR_INST - Sin Acceso UTR')

        
        self.disable_serial_port()

    def ask_then_shutdown(self):
        # apagar pc
        label_font = ('helvetica', 40)
        tk.Label(root, textvariable = time_str, font = label_font, bg = 'white', fg = 'blue', relief = 'raised', bd = 3).pack(fill='x', padx=0, pady=0)
        button = tk.Button(root, text = 'Posponer Apagado', command = lambda: self.shutdown('1min'))
        button.pack()

        self.shutdown()
        root.mainloop()
        

    def disable_serial_port(self, port=0x378):
        # deshabilitar puerto serie
        ctypes.windll.inpout32.Out32(port, 0)

    def enable_serial_port(self, port=0x378):
        ctypes.windll.inpout32.Out32(port, 1)

    def get_daily_data(self, n_days, is_gui_running=False):
        '''Get daily data from all the sensors from n_days until today
        
        	Parameters
        	----------

        	n_days : n previous days until today to get the data from the station
        	is_gui_running  : if False the gui application must be start 
            from the desktop icon, else the programm is running, so you 
            have to use it
        
    	'''
       
        hora_inicio = dt.now().strftime("%H-%M-%S")
        #habilitar puerto serie
        self.enable_serial_port()

        pyautogui.FAILSAFE = False
        dias = timedelta(days = n_days)
        hoy = da.today()
        hoy2 = hoy.strftime("%Y-%m-%d")
        fech = hoy-dias
        fech = fech.strftime("%Y-%m-%d")
        
        if not is_gui_running: 
            # abrir software utr mediante localizacion de icono en el escritorio
            soft = pyautogui.locateOnScreen(self.ruta_icono)
            softx, softy = pyautogui.center(soft)
            pyautogui.doubleClick(softx, softy)
            pyautogui.PAUSE = 3
            
            pyautogui.press("enter")
                   
            pyautogui.typewrite(self.pass_tecmes)
            pyautogui.press("enter")

            #pyautogui.press("enter") #enter del error verificar si esta en la estacion de Santa fe
            pyautogui.press("enter") 
            sleep(10)
            
            pyautogui.PAUSE = 2
            # elegir opción datos
            pyautogui.press("down") 
            pyautogui.press("down")
        else:
            pyautogui.PAUSE = 2
        
        # ----------------Altura-Hidrometrica 
        if not os.path.exists(fech+'_Altura-Hidrometrica.txt'):
            self.get_n_days_data_from_sensor(n_days, 0, fech, '_Altura-Hidrometrica')            
        
        # ----------------Carga de batería
        if not os.path.exists(fech+'_Carga-de-Bateria.txt'):
            self.get_n_days_data_from_sensor(n_days, 1, fech, '_Carga-de-Bateria')
            

        # ----------------Direccion-del-viento
        #if (os.path.exists(fech+'_Direccion-del-viento.txt')==False):
        #    self.get_n_days_data_from_sensor(n,2,fech, '_Direccion-del-viento')
        
        # ----------------Temperatura-del-Aire
        #if (os.path.exists(fech+'_Temperatura-del-Aire.txt')==False):
        #    self.get_n_days_data_from_sensor(n,3,fech, '_Temperatura-del-Aire')    

        # ----------------Humedad-Ambiente
        #if (os.path.exists(fech+'_Humedad-Ambiente.txt')==False):
        #    self.get_n_days_data_from_sensor(n,4,fech, '_Humedad-Ambiente')
        
        # ----------------Altura-Hidrometrica-2 
        #if (os.path.exists(fech+'_Altura-Hidrometrica-2.txt')==False):
        #    self.get_n_days_data_from_sensor(n,5,fech, '_Altura-Hidrometrica-2')

        # ----------------Radiacion-Solar
        #if (os.path.exists(fech+'_Radiacion-Solar.txt')==False):
        #    self.get_n_days_data_from_sensor(n,6,fech, '_Radiacion-Solar')
        
        # ----------------Humedad-del-Suelo
        #if (os.path.exists(fech+'_Humedad-del-Suelo.txt')==False):
        #   self.get_n_days_data_from_sensor(n,7,fech, '_Humedad-del-Suelo')

        # ----------------Temperatura-del-agua
        #if (os.path.exists(fech+'_Temperatura-del-agua.txt')==False):
            #self.get_n_days_data_from_sensor(n,8,fech, '_Temperatura-del-agua')
        
        # ----------------Carga-de-Bateria
        #if (os.path.exists(fech+'_Carga-de-Bateria.txt')==False):
            #self.get_n_days_data_from_sensor(n,9,fech, '_Carga-de-Bateria')

        # ----------------Presion-Atmosferica
        #if (os.path.exists(fech+'_Presion-Atmosferica.txt')==False):
            #self.get_n_days_data_from_sensor(n,10,fech, '_Presion-Atmosferica')
        
        # ----------------Pluviometro
        #if (os.path.exists(fech+'_Pluviometro.txt')==False):
            #self.get_n_days_data_from_sensor(n,11,fech, '_Pluviometro')

        # salir programa
        sleep(2)
        pyautogui.hotkey('alt', 'f4')        
        self.disable_serial_port()        


    def select_previous_n_days(self,n_days):
        '''desplaza n dias atras para obtener los datos diarios

            Parameters
            ----------
            n : indica la cantidad de diás hacia atras a desplazarse, en los campos fechas de la sección datos del software de la UTR
        ''' 
        pyautogui.press("tab")
        for _ in range(1, n_days + 1):
            pyautogui.press("down")
        pyautogui.press("tab")

        for _ in range(1, n_days + 2):
            pyautogui.press("down")
        
    
    def choose_sensor_from_select(self, choosen_sensor):
        '''funcion para elegir sensor del select del software de tecmes

        	Parameters
        	----------
        	
        	n_offset : indica la cantidad de desplazamientos en el select que elige el sensor, 
        	para obtener los datos diarios
        	
        ''' 
        pyautogui.press("tab")
        if choosen_sensor > 0:
            for _ in range(1, choosen_sensor+1):
                pyautogui.press("down")
                
    def save_data(self,fecha,nombre):
        '''funcion que guarda los archivos, datos instantaneos o datos diarios
	
        
        	Parameters
        	----------
        	self : variables para acceder a los atributos y metodos de la clase.
        	fecha : en caso de ser llamada por la funcion get_realtime_data 
        	contiene la fecha actual, con minutos y segundos, en caso de ser 
        	llamada por datos diarios contiene solo la fecha del dia del que 
        	quieren los datos.
        	nombre : en caso de ser llamada por la funcion get_realtime_data 
        	contiene el valor: Datos Instantaneos, en caso de ser llamada por 
        	datos diarios contiene el nombre del sensor.
        	
        	Returns
        	-------
    	''' 
	
        pyautogui.hotkey('ctrl', 'g')
        pyautogui.typewrite(fecha+nombre)
        pyautogui.press("enter")
        logging.info('SAVE - saving data')

    def get_n_days_data_from_sensor(self, n_days, choosen_sensor, fecha,nombre):
        '''funcion que realiza el proceso de elgir sensor y guarda el archivo que se genera

        
        	Parameters
        	----------
        	
        	n_days : indica la cantidad de diás hacia atras a desplazarse, en los 
        	campos fechas de la sección datos del software de la UTR
        	choosen_sensor : indica la cantidad de desplazamientos en el select que elige el 
        	sensor, para obtener los datos diarios		
        	fecha : en caso de ser llamada por la funcion get_realtime_data 
        	contiene la fecha actual, con minutos y segundos, en caso de ser 
        	llamada por datos diarios contiene solo la fecha del 
        	dia del que quieren los datos.
        	nombre : en caso de ser llamada por la funcion get_realtime_data 
        	contiene el valor: Datos Instantaneos, en caso de ser llamada por 
        	datos diarios contiene el nombre del sensor.
        	
        	
    	''' 

        pyautogui.press("enter")
        pyautogui.PAUSE = 1
        self.select_previous_n_days(n_days)

        self.choose_sensor_from_n_day_data(choosen_sensor)
        pyautogui.press("tab")
        pyautogui.press("enter")
        sleep(5)
        
        # guardar
        self.save_data(fecha, nombre)
        
        # salir ventana datos
        pyautogui.press("enter")
        pyautogui.hotkey('alt', 'f4')
        pyautogui.PAUSE = 2

    def is_working_time():
        ''' Check if is the actual time is between the working window 
            
            Returns
            -------
            True or False if the station has been started between the working time. 
            The working time is a class atribute loaded from the config file
        '''
        begin = time(self.hora_inicio_defecto, self.minutos_inicio_defecto, 0)
        end   = time(self.hora_inicio_defecto+1, self.minutos_inicio_defecto+self.delta_inicio_defecto-60, 0)
        now   = dt.time(dt.datetime.now().hour, dt.datetime.now().minute)

        return (begin < now < end)

    def manual_test(self, run = None):
        '''función que da incio al programa
	
        	Parameters
        	----------
        	
        	run : si su valor es None (valor por defecto indica que se debe 
        	respectar la hora de ejecucion, si su valor es test se ejecutan las 
        	tareas sin tener en cuenta el horario establecido, utilizado en 
        	pruebas de campo para comprobar el funcionamiento del programa sin 
        	modificar el archivo de configuración.
        	-------
        ''' 
        # compara la hora actual con el rango en que tiene que ejecutarse la extraccion de datos
        if (self.minutos_inicio_defecto+self.delta_inicio_defecto>=60):
            hora1 = time(self.hora_inicio_defecto, self.minutos_inicio_defecto, 0)
            hora2 = time(self.hora_inicio_defecto+1, self.minutos_inicio_defecto+self.delta_inicio_defecto-60, 0)         
        else:
            hora1 = time(self.hora_inicio_defecto,self.minutos_inicio_defecto, 0)
            hora2 = time(self.hora_inicio_defecto,self.minutos_inicio_defecto+self.delta_inicio_defecto, 0)
        print (hora2)
        ahora = dt.now() 
        hora_actual = time(ahora.hour, ahora.minute, ahora.second)
        ahora_dia = ahora.strftime("%Y-%m-%d")
        ahora_hora = ahora.strftime("%H-%M-%S")
        if run == 'test':
            if (self.opcion_inicio == 1):
                self.get_realtime_data(1)
            elif (self.opcion_inicio == 2):
                self.get_daily_data(self.dias_atras,2)
            elif (self.opcion_inicio == 0):
                self.get_realtime_data(0)
                
                self.get_daily_data(self.dias_atras,0)
        elif (hora_actual < hora2) and (hora_actual > hora1):
                #f = urlopen('http://ceneha.yosobreip.com.ar/estaciones/sah/sfe/opcion.txt')
            

            if (self.opcion_inicio == 1):
                self.get_realtime_data(1)
            elif (self.opcion_inicio == 2):
                self.get_daily_data(self.dias_atras,2)
            elif (self.opcion_inicio == 0):
                self.get_realtime_data(0)
                
                self.get_daily_data(self.dias_atras,0)

        else:

            # label auto-adjusts to the font
            label_font = ('helvetica', 40)
            tk.Label(root, textvariable = time_str, font = label_font, bg = 'white', fg = 'blue', relief = 'raised', bd = 3).pack(fill = 'x', padx = 0, pady = 0)
            # create start and stop buttons
            # pack() positions the buttons below the label
            #tk.Button(root, text='Count Start', command=count_down).pack()
            # stop simply exits root window
            button = tk.Button(root, text='Posponer Apagado', command=lambda : self.shutdown('5min'))
            button.pack()
            # start the GUI event loop
            #w = wmi.WMI(namespace="root\\wmi")
            #temp=w.MSAcpi_ThermalZoneTemperature()[-2].CurrentTemperature
            #temp=str(temp)
            #fo = open("logs.txt", "a")
            #fo.write('Fuera de Hora;  Dia de Ejecucion: '+ahora_dia+';  Hora: '+ahora_hora+'\n')
            logging.warning('OUT- Inicio de programa fuera de hora')
            #fo.close()
            self.upload_data_to_web()
            self.shutdown()
            root.mainloop()
