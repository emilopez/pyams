# -*- coding: utf-8 -*-
from PIL import Image
from datetime import datetime

import logging
import json
import time
import pickle
try:
    import pytesseract
except:
    print('no carga pytesseract')
try:
    import pyautogui
except:
    print('no carga pyautogui')

from win32api import GetSystemMetrics


#import calc
from . import calc

class STATION:

    '''Communicates with the GUI software of Pegasus datalogger.
    '''

    def __init__(self, site=None, config_file = None, rt_app=None, dl_app=None):
        self.realtime_app = rt_app
        self.dlogger_app = dl_app
        self.site = site
        self.screen_resolution = None
        self.img = None
        self.timestamp = None
        self.imgfilename = None
        self.window_position = None
        self.window_size = None
        self.screenshot_box = None
        self.imgs_cropped = {}
        self.__screenshot_fn = None
        self.__timestamp = None
        self.rtdata = {}
        self.acron = {}

        with open(config_file, 'rb') as f: 
            
            config = pickle.load(f)
            self.acron = config['acron']
            self.variables_box  = config['variables_box']

    def is_the_right_resolution(self):
        width, height = GetSystemMetrics(0), GetSystemMetrics(1)
        return self.screen_resolution == (width, height)
        
    def crop_variables_from_screenshot(self):
        ''' Crop the screenshot of the main window of the weather station software	
        
        	Returns
        	-------
            Dict of Image objets corresponding to each cropped variable 
            for test use only
        '''

        logging.info('Start crop_image')
        for v in self.variables_box:
            self.imgs_cropped[v["var"]] = self.img.crop(v["box"])
        logging.info('End crop_image')
        return self.imgs_cropped

    def screenshot(self):
        ''' Obtiene captura de pantalla del soft de monitoreo en tiempo real
        '''
        logging.info('Start captura - getscreenshot')
        self.timestamp = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        if self.imgfilename:
            self.img = Image.open(self.imgfilename)
        else:
            fn = (self.timestamp.replace(':', '-')).replace(' ', '_')
            self.imgfilename = fn + '.jpg'
            self.img = pyautogui.screenshot(self.imgfilename, region=self.screenshot_box)
        logging.info('End captura - getscreenshot')
        return

    def get_raw_realtime_data(self):
        """
        Get raw data from doing OCR to each Image
        """
        text_values = ("ddv", "ddr")
        for k, i in self.imgs_cropped.items():
            '''
            if k in text_values:
                val = pytesseract.image_to_string(i, lang='spa', config="-psm 7")
            else:
                val = pytesseract.image_to_string(i, lang='spa', config="-psm 7 digits")
            '''
            val = pytesseract.image_to_string(i, lang='spa', config="-psm 7")
            self.rtdata[k] = {'desc': self.acron[k],
                              'raw_data': val,
                              'cls_data': None,
                              'cal_data': None}
    def val2number(self, val):
        val = val.replace('e', '3')
        val = val.replace('o', '0')
        val = val.replace(',', '.')
        val_num = val.replace('s', '8')

        return val_num

    def convert_data_to_number(self):
        '''Setea en cls_data del dict la conversion a float si es posible'''

        logging.info('Start set_clean_data')

        # casos no generalizables> ddv, ddr, ras
        self.rtdata['ddv']['cls_data'] = self.rtdata['ddv']['raw_data']
        self.rtdata['ddr']['cls_data'] = self.rtdata['ddr']['raw_data']

        # radiacion solar no es numerico cdo es de noche: "__"
        if self.rtdata['ras']['raw_data'].isnumeric():
            self.rtdata['ras']['cls_data'] = int(self.rtdata['ras']['raw_data'])
            self.rtdata['ras']['cal_data'] = self.rtdata['ras']['cls_data']
        else:
            self.rtdata['ras']['cls_data'] = None
            self.rtdata['ras']['cal_data'] = None

        for k, v in self.rtdata.items():
            if not k in ('ddv', 'timestamp', 'ddr', 'ras'):
                try:
                    v['cls_data'] = float(self.val2number(v['raw_data']))
                except:
                    # debiera loggear
                    print(k, "error val2number", v)
                    v['cls_data'] = None

        logging.info('End set_clean_data')
        return

    def calibrate_data(self, custom=None):
        '''Calibracion y calculo de variables
            - CIM calibra la presion atm
            - ST con eq. del SMN
            - decimales'''
        # sensación térmica (ste)
        T = self.rtdata['tae']['cls_data']
        H = self.rtdata['hae']['cls_data']
        V = self.rtdata['vdv']['cls_data']

        # funcion para calcular ST del SMN
        ste = calc.temp2STSMN(T, H, V)

        # agrego ST al dict
        self.rtdata['ste'] = {'desc': self.acron["ste"],
                                'raw_data': None,
                                'cls_data': None,
                                'cal_data': ste
                                }
        if custom == "cim":
            # 1 decimal: pat,
            self.rtdata['pat']['cal_data'] = round(calc.calib_pat(self.rtdata['pat']['cls_data']), 1)
        else:
            try:
                self.rtdata['pat']['cal_data'] = round(self.rtdata['pat']['cls_data'], 1)
            except:
                self.rtdata['pat']['cal_data'] = self.rtdata['pat']['raw_data']

        # entero: hae, hai, hds
        self.rtdata['hae']['cal_data'] = int(self.rtdata['hae']['cls_data'])
        self.rtdata['hai']['cal_data'] = int(self.rtdata['hai']['cls_data'])
        self.rtdata['hds']['cal_data'] = int(self.rtdata['hds']['cls_data'])

        # 2 decimales: pad,
        self.rtdata['pad']['cal_data'] = round(self.rtdata['pad']['cls_data'], 2)

        # 1 decimal: vdv, idr
        self.rtdata['vdv']['cal_data'] = round(self.rtdata['vdv']['cls_data'], 1)
        self.rtdata['idr']['cal_data'] = round(self.rtdata['idr']['cls_data'], 1)

        # si cal_data es nulo copio lo de cls_data
        if self.rtdata['ras']['raw_data'] == "__":
            self.rtdata['ras']['cls_data'] = None
            self.rtdata['ras']['cal_data'] = None

        for k, v in self.rtdata.items():
            if k is not 'timestamp':
                if v['cal_data'] is None:
                    v['cal_data'] = v['cls_data']


    def save_data(self, data=None):
        '''Agrega data en archivo con fecha del dia aaaa-mm-dd.json'''
        ofname = str(datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')) + '.json'
        if data is None:
            data = self.rtdata
            data['timestamp'] = self.timestamp
        with open(ofname, 'a') as outfile:
            json.dump(data, outfile, indent=4)

    def get_data_ready_to_send(self, data=None):
        '''Retorna datos Ready to Send

           Arma un dict solo con calc_data
        '''
        if data is None:
            data = self.rtdata
        rts = dict()
        #rts['timestamp'] = (data['timestamp'].replace(':', '-')).replace(' ', '_')
        rts['timestamp'] = self.timestamp
        for k, v in data.items():
            if k is not 'timestamp':
                rts[k] = v['cal_data']
        return rts
        
    def set_tessearact_path(path='C:\\Program Files\\Tesseract-OCR\\tesseract'):
        pytesseract.pytesseract.tesseract_cmd = path
