from pyams import FEWS

flesia_station = FEWS.STATION(site='flesia', config_file='flesia_config.cfg')
IPADDR = "173.X.Y.Z"

if flesia_station.is_working_time():
    # enable serial communication, get&save real-time data, disable serial communication
    flesia_station.get_realtime_data()
    # sync data (using rsync) from the station to the server using default settings in config file flesia_config.cfg
    # only rsync protocol was implemented in FEWS station
    flesia_station.upload_data_to_web(server=IPADDR, user="emiliano", password_file="/home/emiliano/credencial.crd")
# shutdown the mini-computer
flesia_station.shutdown()
    