import requests

from pyams import PEGASUS

fca = PEGASUS.STATION(site='Esperanza', config_file='fca_config.pickle')
# resolution of the fca computer
fca.set_screen_resolution(resolution=(1280,1024))
# http server to send data
SERVER = "http://..."
# check if the resolution is right
if fca.is_the_right_resolution():
    # get the screenshot and crop the variables
    fca.crop_variables_from_screenshot()
    # doing OCR from previous images and get the data
    fca.get_raw_realtime_data()
    # convert from str to float  
    fca.convert_data_to_number()
    # do some calcs
    fca.calibrate_data()
    # save data in default dir
    fca.save_data()
    # get data in json structure
    payload = fca.get_data_ready_to_send()
    # send to the http server 
    r = requests.post(SERVER, json=payload)
    