# PyAMS: Python software to Automate Monitoring Stations

PyAMS is a common datalogger interface (CDI) to get data from closed and open monitoring stations (MS). 

<img src="infraestructure.png" width=350 style="float: center; margin-right: 10px;" />

Here you can find examples for the folowing dataloggers or monitoring stations:

- Flood Early Warning System Station (closed)
- Pegasus Weather Station (closed)
- Campbell CR1000/CR3000 (open)
- Stevens Dotlogger (open)

The CDI encapsulates details of each device and gives an abstract method to get the stored data or the real-time meassurments. This methodology is easily replicable to other monitoring stations either with proprietary software when you are generally tied to particular GUI app to drive the device. 

PyAMS was designed using Object Oriented Paradigm, each MS was implemented as a class in the ``pyams`` directory and you can see the examples section to understand how to use it. 

## Add support to a new monitoring station
For the closed MS PyAMS manipulate the GUI proprietary application emulating the manual process. The script control the mouse and keyboard to automate interactions with the application. To add support for a new MS or data-logger you shoud create a new python class in the ``pyams`` directory specifying the methods to get, store and send the data following the examples as a template.

We strongly recommend at least three main method for the new class:
- `get_realtime_data()`: to get the meassurements at the moment
- `get_stored_data()`: to get previously stored meassurements 
- `send_data()`: to send the data to Internet


### Manipulating GUI proprietary application
PyAMS is based in the PyAutoGUI library. You should read the [PyAutoGUI official documentation](https://pyautogui.readthedocs.io/en/latest/ "PyAutoGUI") to understand how to control the mouse and keyboard to automate interactions. 

### Sending data to the web
If the MS store the data in several files may be you prefer post-process  the information on the server side, in that case `rsync` protocol is recommended. You can deploy a rsync server folowing the [rsync official documentation](https://rsync.samba.org/).

By the other way, if you can get the values from each sensor, `http` protocol is recommended. The easiest way is using the `Request` Python library, as you can see in the open devices examples. Please, see the [Request official documentation](https://requests.readthedocs.io/). 

There are many free http servers as [Ubidots](https://ubidots.com/), [Thinger.io](https://thinger.io/) among others. Or you can deploy your own http server using Django or Flask.


In the next section you could see how to use the defined classes for the supported MS previously listed:

## Examples of supported Monitoring Stations

### Flood Early Warning System Station

<img src="fews.jpg" width=250 style="float: left; margin-right: 10px;" />

Examples of getting realtime data for Flesia station 

```python
from pyams import FEWS

# create a station with the config file with the settings parameters
flesia_station = FEWS.STATION(site='flesia', config_file='flesia_config.cfg')
IPADDR = "173.X.Y.Z"

if flesia_station.is_working_time():
    # enable serial communication, get&save real-time data, disable serial communication
    flesia_station.get_realtime_data()
    # only rsync protocol was implemented in FEWS station
    # sync data (using rsync) from the station to the server using default settings in config file flesia_config.cfg for some arguments
    flesia_station.upload_data_to_web(server=IPADDR, user="emiliano", password_file="/home/emiliano/credencial.crd")
# shutdown the mini-computer
flesia_station.shutdown()
```

Get daily data of Flesia Station from sensor 3 and ask for confirmation to shutdown the computer

```python
from pyams import FEWS

# create a station with the config file with the settings parameters
flesia_station = FEWS.STATION(site='flesia', config_file='flesia_config.cfg')

# check if is the right moment
if flesia_station.is_working_time():
    # get daily stored data from 2 days ago until now for the sensor id 3
    flesia_station.get_daily_data(n_days=2, choosen_sensor=3)
    # sync data from the station to the server using default settings
    flesia_station.upload_data_to_web()
flesia_station.ask_then_shutdown()
```

## Pegasus Weather Station

<img src="pegasus.jpg" width=250 style="float: left; margin-right: 10px;" />

Example of automating Pegasus Weather Station from Faculty of Agricultural Science (FCA)

```python
import requests
from pyams import PEGASUS

fca = PEGASUS.STATION(site='Esperanza', config_file='fca_config.pickle')
# resolution of the fca computer
fca.set_screen_resolution(resolution=(1280,1024))
# http server to send data
SERVER = "http://..."
# check if the resolution is right
if fca.is_the_right_resolution():
    # get the screenshot and crop the variables
    fca.crop_variables_from_screenshot()
    # doing OCR from previous images and get the data
    fca.get_raw_realtime_data()
    # convert from str to float  
    fca.convert_data_to_number()
    # do some calcs
    fca.calibrate_data()
    # save data in default dir
    fca.save_data()
    # get data in json structure
    payload = fca.get_data_ready_to_send()
    # send to the http server 
    r = requests.post(SERVER, json=payload)
```

## Campbell Eddy Covariance Station

<img src="campbell_dlogger.png" width=250 style="float: left; margin-right: 10px;" />
<img src="eddy_covariance.jpg" width=250 style="float: left; margin-right: 10px;" />

Campbell and Stevens monitoring station has open dataloggers you can communicate over standard serial port (USB) protocol, each one using their own commands to get realtime and stored data. 

```python
from pyams import OPENDATALOGGER

# instance need serial parameters and datalogger type: CR1000, CR3000 or DOTLOGGER
campbell_cr1000 = OPENDATALOGGER.SERIALDATALOGGER(port='/dev/ttyUSB0', baudrate=115200, type="CR1000")
# start serial connection
campbell_cr1000.start_connection()
# destination path to save data downloaded & removed from datalogger
campbell_cr1000.get_stored_data("/home/emiliano/data.txt")
# close serial connection
campbell_cr1000.close_connection()
```

## Stevens DOTLogger

<img src="stevens.jpeg" width=250 style="float: left; margin-right: 10px;" />

Stevens DOTLogger datalogger real-time example. 

```python
from pyams import OPENDATALOGGER

# instance need serial parameters and datalogger type: CR1000, CR3000 or DOTLOGGER
dotlogger = OPENDATALOGGER.SERIALDATALOGGER(port='/dev/ttyACM0', baudrate=115200, type="DOTLOGGER")
# start serial connection
dotlogger.start_connection()
# Getting real-time data
realtime_data = dotlogger.get_realtime_data()
# close serial connection
dotlogger.close_connection()
# print realtime data
print(realtime_data)
```